import { Keycard } from "../models/Keycard";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Keycard)
class KeycardManager extends Repository<Keycard> {
  createKeycardList(hotelId: string, totalNumberOfRoom: number) {
    const query: string[] = [];
    const keycardList = Array.from(new Array(totalNumberOfRoom)).map((_, index) => {
      const keycardId: string = (index + 1).toString();

      return this.create({
        hotelId,
        id: keycardId
      });
    });

    return this.save(keycardList);
  }

  async assignKeycard(hotelId: string, keycard: Keycard, roomId: string) {
    const registeredKeycard = this.create({
      hotelId,
      roomId,
      id: keycard.id
    })
    
    return this.save(registeredKeycard);
  }

  async dischargeKeycard(hotelId: string, keycard: Keycard) {
    const dischargedKeycard = await this.create({
      hotelId,
      id: keycard.id,
      roomId: null
    })

    return this.save(dischargedKeycard);
  }

  async getAvailableKeycard(hotelId: string): Promise<Keycard> {
    const keycard = await this.findOneOrFail({
      where: {
        hotelId,
        roomId: null
      },
      order: {
        id: 'ASC'
      }
    })

    return keycard;
  }

  async getAllAvailableKeycard(hotelId: string): Promise<Keycard[]> {
    const keycards = await this.find({
      where: {
        hotelId,
        roomId: null
      },
      order: {
        id: 'ASC'
      }
    })

    // if (result.rowCount === 0) {
    //   throw new Error("There is no available keycard");
    // }

    // const rows = result.rows;
    // const keycards = rows.map(
    //   row => this.create({
    //     hotelId: row.hotel_id,
    //     id: row.id,
    //   })
    // );

    return await keycards;
  }

  async getKeycardById(hotelId: string, keycardId: string): Promise<Keycard> {
    const keycard = await this.findOneOrFail({
      id: keycardId,
      hotelId
    })

    return keycard;
  }

  async getKeycardByRoomId(hotelId: string, roomId: string): Promise<Keycard> {
    const keycard = await this.findOneOrFail({
      roomId,
      hotelId
    })

    return keycard;
  }
}

export { KeycardManager };
