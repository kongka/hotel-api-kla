import { Room } from "../models/Room";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Room)
class RoomRepository extends Repository<Room> {
  async getRoomByRoomId(hotelId: string, roomId: string): Promise<Room> {
    const room = await this.findOneOrFail({
      hotelId,
      id: roomId
    })

    return room;
  }

  async getAllAvailableRooms(hotelId: string): Promise<Room[]> {
    const rooms = await this.find({
      hotelId,
      isAvailable: true
    })

    return await rooms;
  }

  async getAllBookedRoomByFloorNumber(hotelId: string, floorNumber: string): Promise<Room[]> {
    const bookedRooms = await this.find({
      isAvailable: false,
      floorNumber: floorNumber,
      hotelId
    }) 
    
    return bookedRooms;
  }

  async getAvailableRoomsByFloor(hotelId: string, floorNumber: string): Promise<Room[]> {
    const rooms = await this.find({
      isAvailable: true,
      floorNumber,
      hotelId
    })
    
    if (rooms.length === 0) {
      throw new Error("There is no available room in this floor");
    }

    return rooms;
  }

  createRooms(hotelId: string, floorCount: number, roomCount: number) {
    const rooms = this.range(1, floorCount).map(floorNumber =>
      this.range(1, roomCount).map(roomNumber => {
        const roomId: string = floorNumber + roomNumber
          .toString()
          .padStart(2, "0");
        
        const room = this.create({
          hotelId,
          id: roomId,
          floorNumber: floorNumber.toString(),
          isAvailable: true
        })
        
        return room
      })
    );
  
    return this.save(rooms.flat());
  }

  range(startAt = 0, size: number) {
    return [...Array(size).keys()].map(i => i + startAt);
  }
}

export { RoomRepository };