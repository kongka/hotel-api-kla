import { Hotel } from "../models/Hotel";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Hotel)
class HotelManager extends Repository<Hotel> {
    async createHotel(id: string, numberOfFloor: string, roomPerFloor: string): Promise<Hotel> {
        const hotel = this.create({
            id,
            numberOfFloor,
            roomPerFloor,
            numberOfRoom: parseInt(numberOfFloor) * parseInt(roomPerFloor)
        });

        return this.save(hotel);
    }
}

export { HotelManager } 