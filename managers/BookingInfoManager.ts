import { BookingInfo } from "../models/BookingInfo";
import { CustomerInfo } from "../models/CustomerInfo";
import { EntityRepository, Repository, getRepository } from "typeorm";

@EntityRepository(BookingInfo)
class BookingInfoManager extends Repository<BookingInfo> {
  async recordBookingInfo(bookingInfo: BookingInfo): Promise<BookingInfo> {
    return await this.save(bookingInfo);
  }

  async removeBookingInfo(hotelId: string, checkoutBookingInfo: BookingInfo): Promise<void> {
    await this.delete({
      hotelId,
      keycardId: checkoutBookingInfo.keycardId
    });
  }

  async getBookingInfoByName(hotelId: string, name: string): Promise<BookingInfo> {
    const bookingInfo = await this.findOneOrFail({
      hotelId,
      guestName: name
    });

    return bookingInfo;
  }

  async getBookingInfoByKeycardId(hotelId: string, keycardId: string): Promise<BookingInfo> {
    const bookingInfo = await this.findOneOrFail({
      hotelId,
      keycardId
    }) 
  
    if (!bookingInfo) {
      throw new Error("There is no booking info with specified name");
    }

    return bookingInfo;
  }

  async insertCustomerInfo(hotelId: string, customerInfo: CustomerInfo) {
    return this.customerInfoRepository.save(customerInfo);
  }

  async removeCustomerInfo(hotelId: string, customerInfo: CustomerInfo) {
    const guests = await this.find({
      hotelId,
      guestName: customerInfo.name,
      guestAge: parseInt(customerInfo.age),
    })

    if (guests.length > 0) {
      return;
    }

    this.customerInfoRepository.delete({
      hotelId,
      name: customerInfo.name,
      age: customerInfo.age
    })
  }
  
  get customerInfoRepository() {
    return getRepository(CustomerInfo);
  }
}

export { BookingInfoManager };