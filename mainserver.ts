import express from 'express';
import * as controller from './controller';
import { hasAuthorization, hasValidHotelId } from './middleware';
import { createConnection } from 'typeorm';
import { config } from 'dotenv';

config();

createConnection().then(() => {
    console.log('Database is connected');

    const app = express();
    const port = process.env.PORT || 3000;

    app.use(express.json());

    function callback() {
        console.log(`Application is running on port ${port}`)
    }

    app.get('/', controller.home);

    app.get('/home', controller.home);

    app.post('/create-hotel', controller.createHotel);

    app.post(
        '/book',
        hasAuthorization,
        hasValidHotelId,
        controller.bookRoom
    );

    app.post(
        '/book-by-floor',
        hasAuthorization,
        hasValidHotelId,
        controller.bookByFloor
    );

    app.post(
        '/checkout',
        hasAuthorization,
        hasValidHotelId,
        controller.checkout
    );

    app.post(
        '/checkout-by-floor',
        hasAuthorization,
        hasValidHotelId,
        controller.checkoutByFloor
    );

    app.get(
        '/rooms',
        hasAuthorization,
        hasValidHotelId,
        controller.listAllRooms
    );

    app.get(
        '/rooms/:id',
        hasAuthorization,
        hasValidHotelId,
        controller.getGuestByRoomId
    );

    app.get(
        '/available-rooms',
        hasAuthorization,
        hasValidHotelId,
        controller.listAvailableRooms
    );

    app.get(
        '/guests',
        hasAuthorization,
        hasValidHotelId,
        controller.listAllGuests
    );

    app.get(
        '/guests/:condition/:age',
        hasAuthorization,
        hasValidHotelId,
        controller.listGuestsByAge
    );

    app.get(
        '/booking-info',
        hasAuthorization,
        hasValidHotelId,
        controller.listAllBookingInfo
    );

    app.listen(port, callback);
}).catch(
    error => {
        console.log(error);
    }
)