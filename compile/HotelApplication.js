"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BookingService_1 = require("./models/BookingService");
const CustomerInfo_1 = require("./models/CustomerInfo");
const RoomManager_1 = require("./managers/RoomManager");
const typeorm_1 = require("typeorm");
const KeycardManager_1 = require("./managers/KeycardManager");
const HotelManager_1 = require("./managers/HotelManager");
const BookingInfoManager_1 = require("./managers/BookingInfoManager");
class HotelApplication {
    async createHotel(id, floorCount, roomCount) {
        const totalNumberOfRooms = parseInt(floorCount) * parseInt(roomCount);
        const hotel = await this.hotelManager.createHotel(id, floorCount, roomCount);
        await this.roomManager.createRooms(id, parseInt(floorCount), parseInt(roomCount));
        await this.keycardManager.createKeycardList(id, totalNumberOfRooms);
        return hotel;
    }
    async getRooms(hotelId) {
        return await this.roomManager.find({
            hotelId
        });
    }
    async getHotels() {
        return await this.hotelManager.find();
    }
    async book(hotelId, roomId, customerInfo) {
        const room = await this.roomManager.getRoomByRoomId(hotelId, roomId);
        const keycard = await this.keycardManager.getAvailableKeycard(hotelId);
        const isHotelFullyBooked = await this.isHotelFullyBooked(hotelId);
        if (isHotelFullyBooked) {
            throw new Error('Hotel is fully booked');
        }
        if (!room.isAvailable) {
            const guestName = await this.getGuestNameByRoomId(hotelId, roomId);
            throw new Error(`Cannot book room ${roomId} for ${customerInfo.name}, the room is currently booked by ${guestName}`);
        }
        const bookingInfo = await BookingService_1.bookingService.bookRoom(hotelId, room, customerInfo, keycard.id);
        await this.keycardManager.assignKeycard(hotelId, keycard, room.id);
        await this.bookingInfoManager.insertCustomerInfo(hotelId, customerInfo);
        return await this.bookingInfoManager.recordBookingInfo(bookingInfo);
    }
    async checkout(hotelId, keycardId, name) {
        const keycard = await this.keycardManager.getKeycardById(hotelId, keycardId);
        const bookingInfo = await this.bookingInfoManager.getBookingInfoByKeycardId(hotelId, keycard.id);
        const checkoutBookingInfo = await this.checkoutRoom(hotelId, name, bookingInfo);
        const customerInfo = typeorm_1.getRepository(CustomerInfo_1.CustomerInfo).create({
            name: bookingInfo.guestName,
            age: bookingInfo.guestAge.toString()
        });
        await this.keycardManager.dischargeKeycard(hotelId, keycard);
        await this.bookingInfoManager.removeBookingInfo(hotelId, bookingInfo);
        await this.bookingInfoManager.removeCustomerInfo(hotelId, customerInfo);
        return await {
            bookingInfo: checkoutBookingInfo,
            checkoutRoom: await this.roomManager.findOneOrFail({
                id: checkoutBookingInfo.roomNumber,
                hotelId
            })
        };
    }
    async bookByFloorNumber(hotelId, floorNumber, customerInfo) {
        const floorIsNotAvailable = !(await this.isFloorAvailable(hotelId, floorNumber));
        if (floorIsNotAvailable) {
            throw new Error(`Cannot book floor ${floorNumber} for ${customerInfo.name}`);
        }
        const availableRoomsOnFloor = await this.roomManager.getAvailableRoomsByFloor(hotelId, floorNumber);
        const keycards = await this.keycardManager.getAllAvailableKeycard(hotelId);
        const keycardIds = await keycards.map(keycard => keycard.id);
        await this.zip(availableRoomsOnFloor, keycards).forEach(([room, keycard]) => {
            this.keycardManager.assignKeycard(hotelId, keycard, room.id);
        });
        return await BookingService_1.bookingService.bookByFloorNumber(hotelId, availableRoomsOnFloor, customerInfo, keycardIds);
    }
    async checkoutByFloorNumber(hotelId, floorNumber) {
        const bookingInfos = await this.getBookingInfosByFloor(hotelId, floorNumber);
        const guestNames = await bookingInfos.map(bookingInfo => bookingInfo.guestName);
        await this.checkoutEachRoom(hotelId, guestNames, bookingInfos);
        return bookingInfos;
    }
    async checkoutEachRoom(hotelId, guestNames, bookingInfos) {
        await this.zip(guestNames, bookingInfos).forEach(async ([guestName, bookingInfo]) => {
            const checkoutBookingInfo = await this.checkoutRoom(hotelId, guestName, bookingInfo);
            const customerInfo = typeorm_1.getRepository(CustomerInfo_1.CustomerInfo).create({
                name: bookingInfo.guestName,
                age: bookingInfo.guestAge.toString()
            });
            await this.bookingInfoManager.removeBookingInfo(hotelId, checkoutBookingInfo);
            await this.bookingInfoManager.removeCustomerInfo(hotelId, customerInfo);
        });
    }
    async checkoutRoom(hotelId, name, bookingInfo) {
        const guestName = bookingInfo.guestName;
        const cannotCheckout = !(await this.canCheckoutRoom(guestName, name));
        if (cannotCheckout) {
            const keycardIdInRoom = bookingInfo.keycardId;
            throw new Error(`Only ${guestName} can checkout with keycard number ${keycardIdInRoom}.`);
        }
        const keycardId = await bookingInfo.keycardId;
        const keycard = await this.keycardManager.getKeycardById(hotelId, keycardId);
        await this.keycardManager.dischargeKeycard(hotelId, keycard);
        const checkoutRoom = await this.roomManager.create({
            hotelId,
            id: bookingInfo.roomNumber,
            isAvailable: true
        });
        await this.roomManager.save(checkoutRoom);
        return await bookingInfo;
    }
    async canCheckoutRoom(guestNameInRoom, name) {
        return await guestNameInRoom === name;
    }
    async getAvailableRooms(hotelId) {
        return await this.roomManager.getAllAvailableRooms(hotelId);
    }
    async getAllGuests(hotelId) {
        const guests = await this.customerRepository.find();
        if (guests.length === 0) {
            throw new Error('There is no customer at this moment');
        }
        return await guests;
    }
    async getAllGuestNames(hotelId) {
        return await this.getAllGuests(hotelId).then((customerInfos) => {
            return customerInfos.map(customerInfo => customerInfo.name);
        });
    }
    async getGuestByRoomId(hotelId, roomId) {
        const bookingInfo = await this.bookingInfoManager.findOneOrFail({
            hotelId,
            roomNumber: roomId
        });
        if (!bookingInfo) {
            throw new Error(`There is no guest staying in room ${roomId}`);
        }
        const customerInfo = this.customerRepository.create({
            hotelId: bookingInfo.hotelId,
            name: bookingInfo.guestName,
            age: bookingInfo.guestAge.toString()
        });
        return await customerInfo;
    }
    async getGuestNameByRoomId(hotelId, roomId) {
        const guest = await this.getGuestByRoomId(hotelId, roomId);
        return guest.name;
    }
    async getGuestsByAge(hotelId, sign, age) {
        const customerInfos = await this.getAllGuests(hotelId);
        const guests = await customerInfos
            .filter((customerInfo) => {
            switch (sign) {
                case "<": {
                    return parseInt(customerInfo.age) < age;
                }
                case ">": {
                    return parseInt(customerInfo.age) > age;
                }
                case "=": {
                    return parseInt(customerInfo.age) === age;
                }
            }
        });
        if (guests.length === 0) {
            throw new Error('No guest found with this age condition');
        }
        return guests;
    }
    async getGuestsByFloorNumber(hotelId, floorNumber) {
        const bookingInfos = await this.getBookingInfosByFloor(hotelId, floorNumber);
        if (bookingInfos.length === 0) {
            throw new Error('There is no customer in this floor');
        }
        const guests = bookingInfos.map((bookingInfo) => this.customerRepository.create({
            hotelId: bookingInfo.hotelId,
            name: bookingInfo.guestName,
            age: bookingInfo.guestAge.toString()
        }));
        return guests;
    }
    async isFloorAvailable(hotelId, floorNumber) {
        const bookedRooms = await this.roomManager.getAllBookedRoomByFloorNumber(hotelId, floorNumber);
        return bookedRooms.length === 0;
    }
    async getBookingInfosByFloor(hotelId, floorNumber) {
        const bookingInfos = await this.bookingInfoManager.find({
            hotelId,
            floorNumber: parseInt(floorNumber)
        });
        if (bookingInfos.length === 0) {
            throw Error('There is no booking info in this floor');
        }
        return await bookingInfos;
    }
    async isHotelFullyBooked(hotelId) {
        const rooms = await this.getAvailableRooms(hotelId);
        return rooms.length === 0;
    }
    async generateId() {
        return Date.now().toString(16);
    }
    zip(a, b) {
        let c = [];
        for (let i = 0; i < a.length; i++) {
            c.push([a[i], b[i]]);
        }
        return c;
    }
    get hotelManager() {
        return typeorm_1.getCustomRepository(HotelManager_1.HotelManager);
    }
    get roomManager() {
        return typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
    }
    get keycardManager() {
        return typeorm_1.getCustomRepository(KeycardManager_1.KeycardManager);
    }
    get bookingInfoManager() {
        return typeorm_1.getCustomRepository(BookingInfoManager_1.BookingInfoManager);
    }
    get customerRepository() {
        return typeorm_1.getRepository(CustomerInfo_1.CustomerInfo);
    }
}
exports.HotelApplication = HotelApplication;
