"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BookingInfo_1 = require("./BookingInfo");
const typeorm_1 = require("typeorm");
const BookingInfoManager_1 = require("../managers/BookingInfoManager");
const RoomManager_1 = require("../managers/RoomManager");
class BookingService {
    async bookRoom(hotelId, room, customerInfo, keycardId) {
        if (!room.isAvailable)
            throw new Error("room is not available");
        const bookingInfoRepo = typeorm_1.getRepository(BookingInfo_1.BookingInfo);
        const bookingInfo = bookingInfoRepo.create({
            hotelId,
            roomNumber: room.id,
            floorNumber: parseInt(room.floorNumber),
            guestName: customerInfo.name,
            guestAge: parseInt(customerInfo.age),
            keycardId
        });
        room.book();
        this.roomManager.save(room);
        return bookingInfo;
    }
    async bookByFloorNumber(hotelId, availableRoomsOnFloor, customerInfo, keycardIds) {
        await this.bookingInfoManager.insertCustomerInfo(hotelId, customerInfo);
        return Promise.all(this.zip(availableRoomsOnFloor, keycardIds)
            .map(([room, keycardId]) => {
            const bookingInfoRepo = typeorm_1.getRepository(BookingInfo_1.BookingInfo);
            const bookingInfo = bookingInfoRepo.create({
                hotelId,
                roomNumber: room.id,
                floorNumber: parseInt(room.floorNumber),
                guestName: customerInfo.name,
                guestAge: parseInt(customerInfo.age),
                keycardId
            });
            this.bookingInfoManager.recordBookingInfo(bookingInfo);
            return this.bookRoom(hotelId, room, customerInfo, keycardId);
        }))
            .then((bookingInfos) => bookingInfos.map(bookingInfo => {
            return {
                bookedRoomIds: bookingInfo.roomNumber,
                keycardIds: bookingInfo.keycardId
            };
        }));
    }
    zip(a, b) {
        let c = [];
        for (let i = 0; i < a.length; i++) {
            c.push([a[i], b[i]]);
        }
        return c;
    }
    get bookingInfoManager() {
        return typeorm_1.getCustomRepository(BookingInfoManager_1.BookingInfoManager);
    }
    get roomManager() {
        return typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
    }
}
exports.BookingService = BookingService;
exports.bookingService = new BookingService();
