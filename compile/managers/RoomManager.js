"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Room_1 = require("../models/Room");
const typeorm_1 = require("typeorm");
let RoomRepository = class RoomRepository extends typeorm_1.Repository {
    async getRoomByRoomId(hotelId, roomId) {
        const room = await this.findOneOrFail({
            hotelId,
            id: roomId
        });
        return room;
    }
    async getAllAvailableRooms(hotelId) {
        const rooms = await this.find({
            hotelId,
            isAvailable: true
        });
        return await rooms;
    }
    async getAllBookedRoomByFloorNumber(hotelId, floorNumber) {
        const bookedRooms = await this.find({
            isAvailable: false,
            floorNumber: floorNumber,
            hotelId
        });
        return bookedRooms;
    }
    async getAvailableRoomsByFloor(hotelId, floorNumber) {
        const rooms = await this.find({
            isAvailable: true,
            floorNumber,
            hotelId
        });
        if (rooms.length === 0) {
            throw new Error("There is no available room in this floor");
        }
        return rooms;
    }
    createRooms(hotelId, floorCount, roomCount) {
        const rooms = this.range(1, floorCount).map(floorNumber => this.range(1, roomCount).map(roomNumber => {
            const roomId = floorNumber + roomNumber
                .toString()
                .padStart(2, "0");
            const room = this.create({
                hotelId,
                id: roomId,
                floorNumber: floorNumber.toString(),
                isAvailable: true
            });
            return room;
        }));
        return this.save(rooms.flat());
    }
    range(startAt = 0, size) {
        return [...Array(size).keys()].map(i => i + startAt);
    }
};
RoomRepository = __decorate([
    typeorm_1.EntityRepository(Room_1.Room)
], RoomRepository);
exports.RoomRepository = RoomRepository;
