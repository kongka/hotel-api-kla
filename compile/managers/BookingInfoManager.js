"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const BookingInfo_1 = require("../models/BookingInfo");
const CustomerInfo_1 = require("../models/CustomerInfo");
const typeorm_1 = require("typeorm");
let BookingInfoManager = class BookingInfoManager extends typeorm_1.Repository {
    async recordBookingInfo(bookingInfo) {
        return await this.save(bookingInfo);
    }
    async removeBookingInfo(hotelId, checkoutBookingInfo) {
        await this.delete({
            hotelId,
            keycardId: checkoutBookingInfo.keycardId
        });
    }
    async getBookingInfoByName(hotelId, name) {
        const bookingInfo = await this.findOneOrFail({
            hotelId,
            guestName: name
        });
        return bookingInfo;
    }
    async getBookingInfoByKeycardId(hotelId, keycardId) {
        const bookingInfo = await this.findOneOrFail({
            hotelId,
            keycardId
        });
        if (!bookingInfo) {
            throw new Error("There is no booking info with specified name");
        }
        return bookingInfo;
    }
    async insertCustomerInfo(hotelId, customerInfo) {
        return this.customerInfoRepository.save(customerInfo);
    }
    async removeCustomerInfo(hotelId, customerInfo) {
        const guests = await this.find({
            hotelId,
            guestName: customerInfo.name,
            guestAge: parseInt(customerInfo.age),
        });
        if (guests.length > 0) {
            return;
        }
        this.customerInfoRepository.delete({
            hotelId,
            name: customerInfo.name,
            age: customerInfo.age
        });
    }
    get customerInfoRepository() {
        return typeorm_1.getRepository(CustomerInfo_1.CustomerInfo);
    }
};
BookingInfoManager = __decorate([
    typeorm_1.EntityRepository(BookingInfo_1.BookingInfo)
], BookingInfoManager);
exports.BookingInfoManager = BookingInfoManager;
