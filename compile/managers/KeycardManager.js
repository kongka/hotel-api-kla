"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Keycard_1 = require("../models/Keycard");
const typeorm_1 = require("typeorm");
let KeycardManager = class KeycardManager extends typeorm_1.Repository {
    createKeycardList(hotelId, totalNumberOfRoom) {
        const query = [];
        const keycardList = Array.from(new Array(totalNumberOfRoom)).map((_, index) => {
            const keycardId = (index + 1).toString();
            return this.create({
                hotelId,
                id: keycardId
            });
        });
        return this.save(keycardList);
    }
    async assignKeycard(hotelId, keycard, roomId) {
        const registeredKeycard = this.create({
            hotelId,
            roomId,
            id: keycard.id
        });
        return this.save(registeredKeycard);
    }
    async dischargeKeycard(hotelId, keycard) {
        const dischargedKeycard = await this.create({
            hotelId,
            id: keycard.id,
            roomId: null
        });
        return this.save(dischargedKeycard);
    }
    async getAvailableKeycard(hotelId) {
        const keycard = await this.findOneOrFail({
            where: {
                hotelId,
                roomId: null
            },
            order: {
                id: 'ASC'
            }
        });
        return keycard;
    }
    async getAllAvailableKeycard(hotelId) {
        const keycards = await this.find({
            where: {
                hotelId,
                roomId: null
            },
            order: {
                id: 'ASC'
            }
        });
        // if (result.rowCount === 0) {
        //   throw new Error("There is no available keycard");
        // }
        // const rows = result.rows;
        // const keycards = rows.map(
        //   row => this.create({
        //     hotelId: row.hotel_id,
        //     id: row.id,
        //   })
        // );
        return await keycards;
    }
    async getKeycardById(hotelId, keycardId) {
        const keycard = await this.findOneOrFail({
            id: keycardId,
            hotelId
        });
        return keycard;
    }
    async getKeycardByRoomId(hotelId, roomId) {
        const keycard = await this.findOneOrFail({
            roomId,
            hotelId
        });
        return keycard;
    }
};
KeycardManager = __decorate([
    typeorm_1.EntityRepository(Keycard_1.Keycard)
], KeycardManager);
exports.KeycardManager = KeycardManager;
