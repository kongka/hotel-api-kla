"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controller = __importStar(require("./controller"));
const middleware_1 = require("./middleware");
const typeorm_1 = require("typeorm");
const dotenv_1 = require("dotenv");
dotenv_1.config();
typeorm_1.createConnection().then(() => {
    console.log('Database is connected');
    const app = express_1.default();
    const port = process.env.PORT || 3000;
    app.use(express_1.default.json());
    function callback() {
        console.log(`Application is running on port ${port}`);
    }
    app.get('/', controller.home);
    app.get('/home', controller.home);
    app.post('/create-hotel', controller.createHotel);
    app.post('/book', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.bookRoom);
    app.post('/book-by-floor', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.bookByFloor);
    app.post('/checkout', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.checkout);
    app.post('/checkout-by-floor', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.checkoutByFloor);
    app.get('/rooms', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.listAllRooms);
    app.get('/rooms/:id', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.getGuestByRoomId);
    app.get('/available-rooms', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.listAvailableRooms);
    app.get('/guests', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.listAllGuests);
    app.get('/guests/:condition/:age', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.listGuestsByAge);
    app.get('/booking-info', middleware_1.hasAuthorization, middleware_1.hasValidHotelId, controller.listAllBookingInfo);
    app.listen(port, callback);
}).catch(error => {
    console.log(error);
});
