"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.hasAuthorization = async (request, response, nextFunction) => {
    if (!request.headers.authorization) {
        return response.status(401).json({
            error: "Not Authroized"
        });
    }
    try {
        const [_, token] = request.headers.authorization.split(' ');
        const decodedData = jsonwebtoken_1.default.verify(token, process.env.APP_KEY);
        request.$payload = decodedData;
        nextFunction();
    }
    catch (error) {
        console.error(error);
        return response.status(401).json({
            error: "Not Authroized"
        });
    }
};
exports.hasValidHotelId = async (request, response, nextFunction) => {
    request.hotelId = request.$payload.hotelId;
    nextFunction();
};
