"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Keycard_1 = require("./models/Keycard");
const db_1 = require("./db");
class KeycardManager {
    createKeycardList(totalNumberOfRoom) {
        const query = [];
        const keycardList = Array.from(new Array(totalNumberOfRoom)).map((_, index) => {
            const keycardId = (index + 1).toString();
            query.push(`('${keycardId}')`);
            return new Keycard_1.Keycard(keycardId);
        });
        db_1.client.query(`INSERT INTO keycards (id) VALUES ${query.join(', ')}`);
        return keycardList;
    }
    async assignKeycard(keycard, roomId) {
        await db_1.client.query(`UPDATE keycards SET room_number = ${roomId} WHERE id = '${keycard.id}'`);
        return keycard.id;
    }
    async dischargeKeycard(keycard) {
        await db_1.client.query(`UPDATE keycards SET room_number = NULL WHERE id = '${keycard.id}'`);
        return await keycard;
    }
    async getAvailableKeycard() {
        const result = await db_1.client.query(`SELECT * FROM keycards WHERE room_number IS NULL ORDER BY id ASC`);
        if (result.rowCount === 0) {
            throw new Error("There is no available keycard");
        }
        const row = result.rows[0];
        const keycard = new Keycard_1.Keycard(row.id);
        return keycard;
    }
    async getAllAvailableKeycard() {
        const result = await db_1.client.query(`SELECT * FROM keycards WHERE room_number IS NULL`);
        if (result.rowCount === 0) {
            throw new Error("There is no available keycard");
        }
        const rows = result.rows;
        const keycards = rows.map(row => new Keycard_1.Keycard(row.id));
        return await keycards;
    }
    async getKeycardById(keycardId) {
        const result = await db_1.client.query(`SELECT * FROM keycards WHERE id = '${keycardId}'`);
        if (result.rowCount === 0) {
            throw new Error("There is no keycard with specified ID");
        }
        const row = result.rows[0];
        const keycard = new Keycard_1.Keycard(row.id);
        return await keycard;
    }
    async getKeycardByRoomId(roomId) {
        const result = await db_1.client.query(`SELECT * FROM keycards WHERE room_id = '${roomId}'`);
        if (result.rowCount === 0) {
            throw new Error("There is no keycard with specified ID");
        }
        const row = result.rows[0];
        const keycard = new Keycard_1.Keycard(row.id);
        return await keycard;
    }
}
exports.KeycardManager = KeycardManager;
exports.keycardManager = new KeycardManager();
