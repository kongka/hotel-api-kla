"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const CustomerInfo_1 = require("./models/CustomerInfo");
const HotelApplication_1 = require("./HotelApplication");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const typeorm_1 = require("typeorm");
const dotenv_1 = require("dotenv");
dotenv_1.config();
const hotelApplication = new HotelApplication_1.HotelApplication();
exports.home = async (request, response, next) => {
    await response.status(200).send('This is Homepage');
};
exports.createHotel = async (request, response, next) => {
    try {
        const hotel = request.body;
        const { numberOfFloor, roomPerFloor } = hotel;
        const id = await hotelApplication.generateId();
        const token = jsonwebtoken_1.default.sign({ hotelId: id }, process.env.APP_KEY, { expiresIn: '1h' });
        await hotelApplication.createHotel(id, numberOfFloor, roomPerFloor);
        response.json({ result: token });
    }
    catch (error) {
        console.error(error);
        response.json({ result: error.message });
    }
};
exports.bookRoom = async (request, response, next) => {
    const requestInfo = request.body;
    const { roomId, name, age } = requestInfo;
    const hotelId = request.hotelId;
    const repository = typeorm_1.getRepository(CustomerInfo_1.CustomerInfo);
    const customerInfo = repository.create({
        hotelId,
        name,
        age
    });
    try {
        const bookingInfo = await hotelApplication.book(hotelId, roomId, customerInfo);
        response.json({
            result: `Room ${bookingInfo.roomNumber} is booked by ${bookingInfo.guestName} with keycard number ${bookingInfo.keycardId}.`
        });
    }
    catch (error) {
        console.error(error);
        response.status(400).json({ result: error.message });
    }
};
exports.checkout = async (request, response, next) => {
    const requestInfo = request.body;
    const { keycardId, name } = requestInfo;
    const hotelId = request.hotelId;
    try {
        const { checkoutRoom } = await hotelApplication.checkout(hotelId, keycardId, name);
        await response.json({
            result: { room: checkoutRoom }
        });
    }
    catch (error) {
        console.error(error);
        await response.status(400).json({ result: error.message });
    }
};
exports.listAllRooms = async (request, response, next) => {
    const hotelId = request.hotelId;
    const rooms = await hotelApplication.getRooms(hotelId);
    await response.status(200).json({ result: rooms });
};
exports.listAllBookingInfo = async (request, response, next) => {
    // const bookingInfo = await hotelApplication.bookingInfoManager!.bookingInfos;
    // response.status(200).json({ result: bookingInfo });
};
exports.listAvailableRooms = async (request, response, next) => {
    const hotelId = request.hotelId;
    try {
        const rooms = await hotelApplication.getAvailableRooms(hotelId);
        await response.status(200).json({ result: rooms });
    }
    catch (error) {
        console.error(error);
        await response.status(404).json({ result: error.message });
    }
};
exports.listAllGuests = async (request, response, next) => {
    const { floorNumber } = request.query;
    const hotelId = request.hotelId;
    try {
        if (floorNumber) {
            const guests = await hotelApplication.getGuestsByFloorNumber(hotelId, floorNumber);
            response.status(200).json({ result: guests });
        }
        const guests = await hotelApplication.getAllGuests(hotelId);
        await response.status(200).json(guests);
    }
    catch (error) {
        console.error(error);
        response.status(404).json(error.message);
    }
};
exports.listGuestsByAge = async (request, response, next) => {
    const params = request.params;
    const hotelId = request.hotelId;
    const guestsNames = await hotelApplication.getGuestsByAge(hotelId, params.condition, parseInt(params.age));
    await response.json(guestsNames);
};
exports.getGuestByRoomId = async (request, response, next) => {
    const { roomId } = request.params;
    const hotelId = request.hotelId;
    try {
        const guest = await hotelApplication.getGuestByRoomId(hotelId, roomId);
        await response.status(200).json({ result: guest });
    }
    catch (error) {
        console.error(error);
        await response.status(404).json({
            result: error.message
        });
    }
};
exports.bookByFloor = async (request, response, next) => {
    const { floorNumber, name, age } = request.body;
    const hotelId = request.hotelId;
    try {
        const repository = typeorm_1.getRepository(CustomerInfo_1.CustomerInfo);
        const customerInfo = repository.create({
            hotelId,
            name,
            age
        });
        const roomsAndKeycardIds = await hotelApplication.bookByFloorNumber(hotelId, floorNumber, customerInfo);
        response.status(200).json({ result: roomsAndKeycardIds });
    }
    catch (error) {
        console.error(error);
        response.status(400).json({ result: error.message });
    }
};
exports.checkoutByFloor = async (request, response, next) => {
    const { floorNumber } = request.body;
    const hotelId = request.hotelId;
    try {
        const bookinginfos = await hotelApplication.checkoutByFloorNumber(hotelId, floorNumber);
        const rooms = bookinginfos.map(bookingInfo => [{
                hotelId: bookingInfo.hotelId,
                roomNumber: bookingInfo.roomNumber,
                floorNumber: bookingInfo.floorNumber
            }]);
        response.status(200).json({ result: rooms });
    }
    catch (error) {
        console.error(error);
        response.status(400).send(error.message);
    }
};
