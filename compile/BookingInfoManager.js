"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BookingInfo_1 = require("./models/BookingInfo");
const db_1 = require("./db");
const Room_1 = require("./Room");
const CustomerInfo_1 = require("./models/CustomerInfo");
class BookingInfoManager {
    async recordBookingInfo(bookingInfo) {
        await db_1.client.query(`INSERT INTO booking_info (room_number, room_floor, guest_name, guest_age, keycard_id)
      VALUES (
        '${bookingInfo.room.id}',
        '${bookingInfo.room.floorNumber}',
        '${bookingInfo.customerInfo.name}',
        '${bookingInfo.customerInfo.age}',
        '${bookingInfo.keycardId}'
      )`);
        return await bookingInfo;
    }
    async removeBookingInfo(checkoutBookingInfo) {
        await db_1.client.query(`DELETE FROM booking_info
      WHERE keycard_id = '${checkoutBookingInfo.keycardId}'`);
    }
    async getBookingInfoByName(name) {
        const result = await db_1.client.query(`SELECT * FROM booking_info
      WHERE guest_name = '${name}'`);
        if (result.rowCount === 0) {
            throw new Error("There is no booking info with specified name");
        }
        const row = result.rows[0];
        const bookingInfo = new BookingInfo_1.BookingInfo(new Room_1.Room(row.room_number, row.room_floor), new CustomerInfo_1.CustomerInfo(row.guest_name, row.guest_age), row.keycard_id);
        return bookingInfo;
    }
    async getBookingInfoByKeycardId(keycardId) {
        const result = await db_1.client.query(`SELECT * FROM booking_info
      WHERE keycard_id = '${keycardId}'`);
        if (result.rowCount === 0) {
            throw new Error("There is no booking info with specified name");
        }
        const row = result.rows[0];
        const bookingInfo = new BookingInfo_1.BookingInfo(new Room_1.Room(row.room_number, row.room_floor), new CustomerInfo_1.CustomerInfo(row.guest_name, row.guest_age), row.keycard_id);
        return bookingInfo;
    }
    async insertCustomerInfo(customerInfo) {
        const guests = await db_1.client.query(`SELECT * FROM booking_info
      WHERE guest_name = '${customerInfo.name}' AND guest_age = '${customerInfo.age}'`);
        if (guests.rowCount <= 1) {
            const query = `INSERT INTO customer_info (name, age) VALUES ('${customerInfo.name}', '${customerInfo.age}')`;
            await db_1.client.query(query);
        }
    }
    async removeCustomerInfo(customerInfo) {
        const guests = await db_1.client.query(`SELECT * FROM booking_info
      WHERE guest_name = '${customerInfo.name}' AND guest_age = '${customerInfo.age}'`);
        if (guests.rowCount > 0) {
            return;
        }
        const query = `DELETE FROM customer_info WHERE name = '${customerInfo.name}' AND age = '${customerInfo.age}'`;
        await db_1.client.query(query);
    }
}
exports.BookingInfoManager = BookingInfoManager;
exports.bookingInfoManager = new BookingInfoManager();
