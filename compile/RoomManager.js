"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Room_1 = require("./models/Room");
const db_1 = require("./db");
class RoomManager {
    async getRoomByRoomId(roomId) {
        const result = await db_1.client.query(`SELECT * FROM rooms WHERE number = '${roomId}'`);
        if (result.rowCount === 0) {
            throw new Error("There is not room");
        }
        const row = result.rows[0];
        const room = new Room_1.Room(row.number, row.floor_number);
        room.isAvailable = row.is_available;
        return await room;
    }
    async getAllAvailableRooms() {
        const result = await db_1.client.query(`SELECT * FROM rooms WHERE is_available = TRUE`);
        if (result.rowCount === 0) {
            throw new Error("There is no room with specified number");
        }
        const rows = result.rows;
        const rooms = await rows.map(row => new Room_1.Room(row.number, row.floor_number));
        return await rooms;
    }
    async getAllBookedRoomByFloorNumber(floorNumber) {
        const result = await db_1.client.query(`SELECT * FROM rooms 
      WHERE is_available = FALSE AND floor_number = '${floorNumber}'`);
        const rows = result.rows;
        const rooms = rows.map(row => new Room_1.Room(row.number, row.floor_number));
        return await rooms;
    }
    async getAvailableRoomsByFloor(floorNumber) {
        const result = await db_1.client.query(`SELECT * FROM rooms 
      WHERE is_available = TRUE AND floor_number = '${floorNumber}'`);
        if (result.rowCount === 0) {
            throw new Error("There is no available room in this floor");
        }
        const rows = result.rows;
        const rooms = rows.map(row => new Room_1.Room(row.number, row.floor_number));
        return await rooms;
    }
    createRooms(floorCount, roomCount) {
        const query = [];
        const rooms = this.range(1, floorCount).map(floorNumber => this.range(1, roomCount).map(roomNumber => {
            const roomId = floorNumber + roomNumber.toString().padStart(2, "0");
            query.push(`('${roomId}', '${floorNumber}')`);
            return new Room_1.Room(roomId, floorNumber.toString());
        }));
        db_1.client.query(`INSERT INTO rooms (number, floor_number) VALUES ${query}`);
        return rooms;
    }
    range(startAt = 0, size) {
        return [...Array(size).keys()].map(i => i + startAt);
    }
}
exports.RoomManager = RoomManager;
exports.roomManager = new RoomManager();
