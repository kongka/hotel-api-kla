module.exports = {
    type: 'postgres',
    url: process.env.DATABASE_URL,
    entities: ['compile/models/*.js']
  }
  