// import { readFileSync } from "fs";
// import { Command } from "./models/Command";
// import { CustomerInfo } from "./models/CustomerInfo";
// import { HotelApplication, client } from "./HotelApplication";
// import { getKeycards } from "./db";

// export const hotelApplication = new HotelApplication();

// export async function main(): Promise<HotelApplication> {
//   const inputFile = "input.txt";
//   const commands = await getCommandFromInputFile(inputFile);

//   await commands.forEach(async (command) => {
//     switch (command.name) {
//       case "create_hotel": {
//         const [floorCount, roomCount] = command.params;

//         await hotelApplication.createHotel(floorCount, roomCount);

//         await console.log(
//           `Hotel created with ${floorCount} floor(s), ${roomCount} room(s) per floor.`
//         );
//         break;
//       }

//       case "book": {
//         const [roomId, name, age] = await command.params;
//         const customerInfo = await new CustomerInfo(name, age);

//         try {
//           const bookingInfo = await hotelApplication.book(roomId, customerInfo);

//           await console.log(
//             `Room ${bookingInfo.room.id} is booked by ${
//             bookingInfo.customerInfo.name
//             } with keycard number ${bookingInfo.keycardId}.`
//           );
//         } catch (err) {
//           console.log(err.message);
//         }

//         break;
//       }

//       case "checkout": {
//         const [keycardId, name] = command.params;

//         try {
//           const checkoutBookingInfo = await hotelApplication.checkout(
//             keycardId,
//             name
//           );

//           await console.log(`Room ${checkoutBookingInfo.room.id} is checkout.`);
//         } catch (err) {
//           console.log(err.message);
//         }
//         break;
//       }

//       case "list_available_rooms": {
//         const availableRooms = await hotelApplication.getAvailableRooms();
//         const roomIds = await availableRooms.map(availableRoom => availableRoom.id);

//         await console.log(roomIds.join());
//         break;
//       }

//       case "list_guest": {
//         const guestNames = await hotelApplication.getAllGuestNames();

//         await console.log(guestNames.join());
//         break;
//       }

//       case "get_guest_in_room": {
//         const [roomId] = command.params;
//         const guest = await hotelApplication.getGuestNameByRoomId(roomId);

//         await console.log(guest);
//         break;
//       }

//       case "list_guest_by_age": {
//         const [sign, age] = command.params;
//         const guestNames = await hotelApplication.getGuestNamesByAge(
//           sign,
//           parseInt(age)
//         );

//         await console.log(guestNames.join());

//         break;
//       }

//       case "list_guest_by_floor": {
//         const [floorNumber] = command.params;
//         const guests = await hotelApplication.getGuestsByFloorNumber(floorNumber);
//         const guestNames = await guests.map(guest => guest.name);

//         await console.log(guestNames.join());

//         break;
//       }

//       case "checkout_guest_by_floor": {
//         const [floorNumber] = command.params;
//         const roomIds = await hotelApplication.checkoutByFloorNumber(floorNumber);

//         await console.log(`Room ${roomIds} are checkout.`);

//         break;
//       }

//       case "book_by_floor": {
//         const [floorNumber, name, age] = command.params;

//         try {
//           const customerInfo = new CustomerInfo(name, age);

//           const roomsAndKeycards = await hotelApplication.bookByFloorNumber(
//             floorNumber,
//             customerInfo
//           );
//           const bookedRoomIds = await roomsAndKeycards.map(
//             roomAndKeycard => roomAndKeycard.bookedRoomIds
//           );
//           const keycardIds = await roomsAndKeycards.map(
//             roomAndKeycard => roomAndKeycard.keycardIds
//           );

//           await console.log(
//             `Room ${bookedRoomIds.join()} are booked with keycard number${keycardIds.join()}`
//           );
//         } catch (err) {
//           console.log(err.message);
//         }
//         break;
//       }

//       default:
//         return;
//     }
//   });

//   return hotelApplication;
// }

// function getCommandFromInputFile(inputFile: string): Command[] {
//   return readFileSync(inputFile, "utf-8")
//     .split("\r\n")
//     .map(line => line.split(" "))
//     .map(([name, ...params]) => new Command(name, params));
// }

// main();
