import { RequestHandler, NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

export const hasAuthorization: RequestHandler = async (
    request: Request,
    response: Response,
    nextFunction: NextFunction
) => {
    if (!request.headers.authorization) {
        return response.status(401).json({
            error: "Not Authroized"
        })
    }

    try {
        const [_, token] = request.headers.authorization!.split(' ');
        const decodedData = jwt.verify(
            token,
            process.env.APP_KEY as string
        );
        (request as any).$payload = decodedData;

        nextFunction();
    } catch (error) {
        console.error(error);
        return response.status(401).json({
            error: "Not Authroized"
        })
    }
}

export const hasValidHotelId: RequestHandler = async (
    request: Request,
    response: Response,
    nextFunction: NextFunction
) => {
    (request as any).hotelId = (request as any).$payload.hotelId;

    nextFunction();
}