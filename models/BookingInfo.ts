import { Room } from "./Room";
import { CustomerInfo } from "./CustomerInfo";
import { Entity, PrimaryColumn, Column, JoinColumn } from "typeorm";

@Entity('booking_info')
class BookingInfo {
  @JoinColumn({ name: 'room_number' })
  room: Room;
  customerInfo: CustomerInfo;
  
  @PrimaryColumn({ name: 'hotel_id' })
  hotelId: string;

  @PrimaryColumn({ name: 'keycard_id' })
  keycardId: string;

  @Column({ name: 'guest_name' })
  guestName: string;

  @Column({ name: 'guest_age' })
  guestAge: number;

  @Column({ name: 'room_number' })
  roomNumber: string;
  
  @Column({ name: 'room_floor' })
  floorNumber: number;

  // constructor(hotelId: string, room: Room, customerInfo: CustomerInfo, keycardId: string) {
  //   this.hotelId = hotelId;
  //   this.room = room;
  //   this.roomNumber = room.id;
  //   this.customerInfo = customerInfo;
  //   this.guestName = customerInfo.name;
  //   this.guestAge = parseInt(customerInfo.age)
  //   this.keycardId = keycardId;
  // }
}

export { BookingInfo };
