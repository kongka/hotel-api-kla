import { Entity, PrimaryColumn, Column, OneToMany } from "typeorm";
import { Room } from "./Room";

@Entity('hotels')
export class Hotel {
    @PrimaryColumn({ name: 'id' })
    id?: string

    @Column({ name: 'number_of_floor' })
    numberOfFloor?: string

    @Column({ name: 'room_per_floor' })
    roomPerFloor?: string

    @Column({ name: 'number_of_room' })
    numberOfRoom?: number

    // constructor(id: string, numberOfFloor: string, roomPerFloor: string) {
    //     this.id = id;
    //     this.numberOfFloor = numberOfFloor;
    //     this.roomPerFloor = roomPerFloor;
    //     this.numberOfRoom = parseInt(numberOfFloor) * parseInt(roomPerFloor);
    // }
}