import { Entity, PrimaryColumn, Column } from "typeorm";

@Entity('keycards')
class Keycard {
  @PrimaryColumn({ name: 'hotel_id' })
  hotelId: string;
  
  @PrimaryColumn({ name: 'id' })
  id: string;

  @Column({ type: 'text', name: 'room_number', nullable: true })
  roomId: string | null;
}

export { Keycard };