import { Entity, Column, PrimaryColumn } from "typeorm";

@Entity('customer_info')
class CustomerInfo {
  @Column({ name: 'hotel_id' })
  hotelId: string;

  @PrimaryColumn({ name: 'name' })
  name: string;

  @PrimaryColumn({ name: 'age' })
  age: string;

  // constructor(hotelId: string, name: string, age: string) {
  //   this.hotelId = hotelId;
  //   this.name = name;
  //   this.age = age;
  // }
}

export { CustomerInfo };
