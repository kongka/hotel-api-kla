import { Entity, PrimaryColumn, Column, JoinColumn, ManyToOne } from "typeorm";
import { Hotel } from "./Hotel";

@Entity('rooms')
class Room {
  @PrimaryColumn({ name: 'hotel_id' })
  hotelId: string

  @PrimaryColumn({ name: 'number' })
  id: string

  @Column({ name: 'floor_number' })
  floorNumber: string

  @Column({ name: 'is_available' })
  isAvailable: boolean

  // constructor(hotelId: string, id: string, floorNumber: string) {
  //   this.hotelId = hotelId;
  //   this.id = id;
  //   this.floorNumber = floorNumber;
  //   this.isAvailable = true;
  // }

  book(): void {
    this.isAvailable = false;
  }

  checkout(): void {
    this.isAvailable = true;
  }
}

export { Room };
