import { BookingInfo } from "./BookingInfo";
import { Room } from "./Room";
import { CustomerInfo } from "./CustomerInfo";
import { getRepository, getCustomRepository } from "typeorm";
import { BookingInfoManager } from "../managers/BookingInfoManager";
import { RoomRepository } from "../managers/RoomManager";

class BookingService {
  async bookRoom(
    hotelId: string,
    room: Room,
    customerInfo: CustomerInfo,
    keycardId: string
  ) {
    if (!room.isAvailable) throw new Error("room is not available");

    const bookingInfoRepo = getRepository(BookingInfo);
    const bookingInfo = bookingInfoRepo.create({
        hotelId,
        roomNumber: room.id,
        floorNumber: parseInt(room.floorNumber),
        guestName: customerInfo.name,
        guestAge: parseInt(customerInfo.age),
        keycardId
    })

    room.book();
    this.roomManager.save(room);
    
    return bookingInfo;
  }

  async bookByFloorNumber(
    hotelId: string,
    availableRoomsOnFloor: Room[],
    customerInfo: CustomerInfo,
    keycardIds: string[]
  ): Promise<{ bookedRoomIds: string; keycardIds: string }[]> {
    await this.bookingInfoManager.insertCustomerInfo(hotelId, customerInfo);

    return Promise.all(this.zip(availableRoomsOnFloor, keycardIds)
      .map(([room, keycardId]) => {
        const bookingInfoRepo = getRepository(BookingInfo);
        const bookingInfo = bookingInfoRepo.create({
          hotelId,
          roomNumber: room.id,
          floorNumber: parseInt(room.floorNumber),
          guestName: customerInfo.name,
          guestAge: parseInt(customerInfo.age),
          keycardId
        })

        this.bookingInfoManager.recordBookingInfo(bookingInfo);

        return this.bookRoom(hotelId, room, customerInfo, keycardId);
      }))
      .then(
        (bookingInfos: BookingInfo[]) => bookingInfos.map(
          bookingInfo => {
            return {
              bookedRoomIds: bookingInfo.roomNumber,
              keycardIds: bookingInfo.keycardId
            }
          }
      ));
  }

  zip<T, G>(a: T[], b: G[]): [T, G][] {
    let c = [];
    for (let i = 0; i < a.length; i++) {
      c.push([a[i], b[i]]);
    }
    return c as any;
  }

  get bookingInfoManager() {
    return getCustomRepository(BookingInfoManager);
  }

  get roomManager() {
    return getCustomRepository(RoomRepository);
  }
}

export { BookingService };
export const bookingService = new BookingService(); 
