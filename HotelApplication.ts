import { Room } from "./models/Room";
import { BookingInfo } from "./models/BookingInfo";
import { bookingService } from "./models/BookingService"
import { CustomerInfo } from "./models/CustomerInfo";
import { Hotel } from "./models/Hotel";
import { RoomRepository } from "./managers/RoomManager";
import { getCustomRepository, getRepository } from "typeorm";
import { KeycardManager } from "./managers/KeycardManager";
import { HotelManager } from "./managers/HotelManager";
import { BookingInfoManager } from "./managers/BookingInfoManager";

class HotelApplication {
  async createHotel(id: string, floorCount: string, roomCount: string): Promise<Hotel> {
    const totalNumberOfRooms = parseInt(floorCount) * parseInt(roomCount)

    const hotel = await this.hotelManager.createHotel(id, floorCount, roomCount)
    await this.roomManager.createRooms(
      id,
      parseInt(floorCount),
      parseInt(roomCount)
    );
    await this.keycardManager.createKeycardList(id, totalNumberOfRooms);

    return hotel;
  }

  async getRooms(hotelId: string) {
    return await this.roomManager.find({
      hotelId
    })
  }

  async getHotels() {
    return await this.hotelManager.find();
  }

  async book(hotelId: string, roomId: string, customerInfo: CustomerInfo): Promise<BookingInfo> {
    const room = await this.roomManager.getRoomByRoomId(hotelId, roomId);
    const keycard = await this.keycardManager.getAvailableKeycard(hotelId);
    const isHotelFullyBooked = await this.isHotelFullyBooked(hotelId)

    if (isHotelFullyBooked) {
      throw new Error('Hotel is fully booked');
    }
    if (!room.isAvailable) {
      const guestName = await this.getGuestNameByRoomId(hotelId, roomId)
      throw new Error(`Cannot book room ${roomId} for ${
        customerInfo.name}, the room is currently booked by ${guestName}`
      );
    }

    const bookingInfo = await bookingService.bookRoom(
      hotelId,
      room,
      customerInfo,
      keycard.id
    );

    await this.keycardManager.assignKeycard(hotelId, keycard, room.id);
    await this.bookingInfoManager.insertCustomerInfo(hotelId, customerInfo);

    return await this.bookingInfoManager.recordBookingInfo(bookingInfo);
  }

  async checkout(hotelId: string, keycardId: string, name: string): Promise<{
    bookingInfo: BookingInfo;
    checkoutRoom: Room
  }> {
    const keycard = await this.keycardManager.getKeycardById(hotelId, keycardId);
    const bookingInfo = await this.bookingInfoManager.getBookingInfoByKeycardId(
      hotelId,
      keycard.id
    );
    const checkoutBookingInfo = await this.checkoutRoom(hotelId, name, bookingInfo);
    const customerInfo = getRepository(CustomerInfo).create({
      name: bookingInfo.guestName,
      age: bookingInfo.guestAge.toString()
    })

    await this.keycardManager.dischargeKeycard(hotelId, keycard);
    await this.bookingInfoManager.removeBookingInfo(hotelId, bookingInfo);
    await this.bookingInfoManager.removeCustomerInfo(hotelId, customerInfo);

    return await {
      bookingInfo: checkoutBookingInfo,
      checkoutRoom: await this.roomManager.findOneOrFail({
        id: checkoutBookingInfo.roomNumber,
        hotelId
      })
    };
  }

  async bookByFloorNumber(hotelId: string, floorNumber: string, customerInfo: CustomerInfo) {
    const floorIsNotAvailable = !(await this.isFloorAvailable(hotelId, floorNumber));

    if (floorIsNotAvailable) {
      throw new Error(
        `Cannot book floor ${floorNumber} for ${customerInfo.name}`
      );
    }

    const availableRoomsOnFloor = await this.roomManager.getAvailableRoomsByFloor(
      hotelId,
      floorNumber
    );
    const keycards = await this.keycardManager.getAllAvailableKeycard(hotelId);
    const keycardIds = await keycards.map(keycard => keycard.id);

    await this.zip(availableRoomsOnFloor, keycards).forEach(([room, keycard]) => {
      this.keycardManager.assignKeycard(hotelId, keycard, room.id);
    });

    return await bookingService.bookByFloorNumber(
      hotelId,
      availableRoomsOnFloor,
      customerInfo,
      keycardIds
    );
  }

  async checkoutByFloorNumber(hotelId: string, floorNumber: string): Promise<BookingInfo[]> {
    const bookingInfos = await this.getBookingInfosByFloor(hotelId, floorNumber);
    const guestNames = await bookingInfos.map(
      bookingInfo => bookingInfo.guestName
    );

    await this.checkoutEachRoom(hotelId, guestNames, bookingInfos);

    return bookingInfos;
  }

  async checkoutEachRoom(hotelId: string, guestNames: string[], bookingInfos: BookingInfo[]) {
    await this.zip<string, BookingInfo>(guestNames, bookingInfos).forEach(
      async ([guestName, bookingInfo]) => {
        const checkoutBookingInfo = await this.checkoutRoom(hotelId, guestName, bookingInfo);
        const customerInfo = getRepository(CustomerInfo).create({
          name: bookingInfo.guestName,
          age: bookingInfo.guestAge.toString()
        });

        await this.bookingInfoManager.removeBookingInfo(hotelId, checkoutBookingInfo);
        await this.bookingInfoManager.removeCustomerInfo(hotelId, customerInfo);
      }
    );
  }

  async checkoutRoom(hotelId: string, name: string, bookingInfo: BookingInfo) {
    const guestName = bookingInfo.guestName;
    const cannotCheckout = !(await this.canCheckoutRoom(guestName, name))

    if (cannotCheckout) {
      const keycardIdInRoom = bookingInfo.keycardId;
      throw new Error(
        `Only ${guestName} can checkout with keycard number ${keycardIdInRoom}.`
      );
    }

    const keycardId = await bookingInfo.keycardId;
    const keycard = await this.keycardManager.getKeycardById(hotelId, keycardId);

    await this.keycardManager.dischargeKeycard(hotelId, keycard);

    const checkoutRoom = await this.roomManager.create({
      hotelId,
      id: bookingInfo.roomNumber,
      isAvailable: true
    })

    await this.roomManager.save(checkoutRoom)

    return await bookingInfo;
  }

  async canCheckoutRoom(guestNameInRoom: string, name: string) {
    return await guestNameInRoom === name;
  }

  async getAvailableRooms(hotelId: string): Promise<Room[]> {
    return await this.roomManager.getAllAvailableRooms(hotelId);
  }

  async getAllGuests(hotelId: string): Promise<CustomerInfo[]> {
    const guests = await this.customerRepository.find()
  
    if (guests.length === 0) {
      throw new Error('There is no customer at this moment');
    }

    return await guests;
  }

  async getAllGuestNames(hotelId: string): Promise<string[]> {
    return await this.getAllGuests(hotelId).then(
      (customerInfos: CustomerInfo[]) => {
        return customerInfos.map(customerInfo => customerInfo.name)
      })
  }

  async getGuestByRoomId(hotelId: string, roomId: string): Promise<CustomerInfo> {
    const bookingInfo = await this.bookingInfoManager.findOneOrFail({
      hotelId,
      roomNumber: roomId
    }) 
    
    if (!bookingInfo) {
      throw new Error(`There is no guest staying in room ${roomId}`);
    }

    const customerInfo = this.customerRepository.create({
      hotelId: bookingInfo.hotelId,
      name: bookingInfo.guestName,
      age: bookingInfo.guestAge.toString()
    });

    return await customerInfo;
  }

  async getGuestNameByRoomId(hotelId: string, roomId: string): Promise<string> {
    const guest = await this.getGuestByRoomId(hotelId, roomId);
    return guest.name;
  }

  async getGuestsByAge(hotelId: string, sign: string, age: number): Promise<CustomerInfo[]> {
    const customerInfos = await this.getAllGuests(hotelId);
    const guests = await customerInfos
      .filter((customerInfo: CustomerInfo) => {
        switch (sign) {
          case "<": {
            return parseInt(customerInfo.age) < age;
          }
          case ">": {
            return parseInt(customerInfo.age) > age;
          }
          case "=": {
            return parseInt(customerInfo.age) === age;
          }
        }
      })

    if (guests.length === 0) {
      throw new Error('No guest found with this age condition');
    }

    return guests;
  }

  async getGuestsByFloorNumber(hotelId: string, floorNumber: string): Promise<CustomerInfo[]> {
    const bookingInfos = await this.getBookingInfosByFloor(hotelId, floorNumber)
    
    if (bookingInfos.length === 0) {
      throw new Error('There is no customer in this floor');
    }

    const guests = bookingInfos.map(
      (bookingInfo: BookingInfo) => this.customerRepository.create({
        hotelId: bookingInfo.hotelId,
        name: bookingInfo.guestName,
        age: bookingInfo.guestAge.toString()
      })
    );

    return guests;
  }

  async isFloorAvailable(hotelId: string, floorNumber: string): Promise<boolean> {
    const bookedRooms = await this.roomManager.getAllBookedRoomByFloorNumber(hotelId, floorNumber)

    return bookedRooms.length === 0;
  }

  async getBookingInfosByFloor(hotelId: string, floorNumber: string): Promise<BookingInfo[]> {
    const bookingInfos = await this.bookingInfoManager.find({
      hotelId,
      floorNumber: parseInt(floorNumber)
    })

    if (bookingInfos.length === 0) {
      throw Error('There is no booking info in this floor');
    }

    return await bookingInfos;
  }

  async isHotelFullyBooked(hotelId: string): Promise<boolean> {
    const rooms = await this.getAvailableRooms(hotelId);

    return rooms.length === 0;
  }

  async generateId(): Promise<string> {
    return Date.now().toString(16);
  }

  zip<T, G>(a: T[], b: G[]): [T, G][] {
    let c = [];
    for (let i = 0; i < a.length; i++) {
      c.push([a[i], b[i]]);
    }
    return c as any;
  }

  get hotelManager() {
    return getCustomRepository(HotelManager)
  }

  get roomManager() {
    return getCustomRepository(RoomRepository);
  }

  get keycardManager() {
    return getCustomRepository(KeycardManager)
  }

  get bookingInfoManager() {
    return getCustomRepository(BookingInfoManager);
  }

  get customerRepository() {
    return getRepository(CustomerInfo);
  }
}

export { HotelApplication };
