import { RequestHandler, Request, Response, NextFunction } from 'express';
import { CustomerInfo } from './models/CustomerInfo';
import { HotelApplication } from "./HotelApplication"
import jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import { config } from 'dotenv';

config();

const hotelApplication = new HotelApplication();

export const home: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    await response.status(200).send('This is Homepage');
}

export const createHotel: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    try {
        const hotel = request.body;
        const { numberOfFloor, roomPerFloor } = hotel;
        const id: string = await hotelApplication.generateId();
        const token = jwt.sign(
            { hotelId: id },
            process.env.APP_KEY as string, 
            { expiresIn: '1h' }
        );
        await hotelApplication.createHotel(id, numberOfFloor, roomPerFloor);
    
        response.json({ result: token });
    } catch (error) {
        console.error(error);
        response.json({ result: error.message })
    }
}

export const bookRoom: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const requestInfo = request.body;
    const { roomId, name, age } = requestInfo;
    const hotelId: string = (request as any).hotelId
    const repository = getRepository(CustomerInfo)
    const customerInfo = repository.create({
        hotelId,
        name,
        age
    })

    try {
        const bookingInfo = await hotelApplication.book(
            hotelId,
            roomId,
            customerInfo
        );

        response.json(
            {
                result: `Room ${bookingInfo.roomNumber} is booked by ${
                    bookingInfo.guestName
                    } with keycard number ${bookingInfo.keycardId}.`
            }
        );
    } catch (error) {
        console.error(error);
        response.status(400).json({ result: error.message });
    }
}

export const checkout: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const requestInfo = request.body;
    const { keycardId, name } = requestInfo;
    const hotelId: string = (request as any).hotelId

    try {
        const { checkoutRoom } = await hotelApplication.checkout(
            hotelId,
            keycardId,
            name
        );

        await response.json({
            result: { room: checkoutRoom }
        });
    } catch (error) {
        console.error(error);
        await response.status(400).json({ result: error.message });
    }
}

export const listAllRooms: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const hotelId = (request as any).hotelId
    const rooms = await hotelApplication.getRooms(hotelId);
    await response.status(200).json({ result: rooms });
}

export const listAllBookingInfo: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    // const bookingInfo = await hotelApplication.bookingInfoManager!.bookingInfos;

    // response.status(200).json({ result: bookingInfo });
}

export const listAvailableRooms: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const hotelId = (request as any).hotelId

    try {
        const rooms  = await hotelApplication.getAvailableRooms(hotelId);

        await response.status(200).json({ result: rooms });
    } catch (error) {
        console.error(error);
        await response.status(404).json({ result: error.message });
    }
}

export const listAllGuests: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const { floorNumber } = request.query;
    const hotelId = (request as any).hotelId

    try {
        if (floorNumber) {
            const guests = await hotelApplication.getGuestsByFloorNumber(hotelId, floorNumber);

            response.status(200).json({ result: guests });
        }

        const guests = await hotelApplication.getAllGuests(hotelId);

        await response.status(200).json(guests);
    } catch (error) {
        console.error(error);
        response.status(404).json(error.message)
    }
}

export const listGuestsByAge: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const params = request.params;
    const hotelId = (request as any).hotelId
    const guestsNames = await hotelApplication.getGuestsByAge(
        hotelId,
        params.condition,
        parseInt(params.age)
    );

    await response.json(guestsNames);
}

export const getGuestByRoomId: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const { roomId } = request.params;
    const hotelId = (request as any).hotelId

    try {
        const guest: CustomerInfo = await hotelApplication.getGuestByRoomId(hotelId, roomId);

        await response.status(200).json({ result: guest });
    } catch (error) {
        console.error(error);
        await response.status(404).json({
            result: error.message
        });
    }
}

export const bookByFloor: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const { floorNumber, name, age } = request.body;
    const hotelId = (request as any).hotelId

    try {
        const repository = getRepository(CustomerInfo)
        const customerInfo = repository.create({
            hotelId,
            name,
            age
        });
        const roomsAndKeycardIds = await hotelApplication.bookByFloorNumber(hotelId, floorNumber, customerInfo);

        response.status(200).json({ result: roomsAndKeycardIds });
    } catch (error) {
        console.error(error);
        response.status(400).json({ result: error.message });
    }
}

export const checkoutByFloor: RequestHandler = async (
    request: Request,
    response: Response,
    next: NextFunction
) => {
    const { floorNumber } = request.body;
    const hotelId = (request as any).hotelId

    try {
        const bookinginfos = await hotelApplication.checkoutByFloorNumber(hotelId, floorNumber);
        const rooms = bookinginfos.map(
            bookingInfo => [{
                hotelId: bookingInfo.hotelId,
                roomNumber: bookingInfo.roomNumber,
                floorNumber: bookingInfo.floorNumber
            }]
        );

        response.status(200).json({ result: rooms })
    } catch (error) {
        console.error(error);
        response.status(400).send(error.message);
    }
}